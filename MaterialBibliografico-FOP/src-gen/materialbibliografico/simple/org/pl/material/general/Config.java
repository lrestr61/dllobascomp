package materialbibliografico.simple.org.pl.material.general;

/*** added by dConfig
 */
public class Config {
	public static final String SERVIDOR = "127.0.0.1";
	public static final int PUERTO = 3306;
	public static final String BD = "bibliotecadb";
	public static final String NOMBRE_USUARIO = "root";
	public static final String CONTRASENA_USUARIO = "";
}