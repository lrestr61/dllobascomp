package materialbibliografico.simple.org.pl.material.dto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import materialbibliografico.simple.org.pl.material.bd.Conexion;
/*** added by dMaterialDAOMySQL* modified by dListarMaterial
 */
public class MaterialDAOMySQL {
	static Statement st = null;
	PreparedStatement ps = null;
	static ResultSet rs = null;
	static Connection conn = null;
	Conexion conexion = null;
	/*** added by dListarMaterial
	 */
	public static Object [] [] obtenerTodas() {
		try {
			int count1 = 0;
			conn = Conexion.getConexion();
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM materialbibliografico");
			Object [] [] ArrayNew = new Object[30][5];
			while(rs.next()) {
				ArrayNew[count1][0] = rs.getInt(1);
				ArrayNew[count1][1] = rs.getString(2);
				ArrayNew[count1][2] = rs.getInt(3);
				ArrayNew[count1][3] = rs.getString(4);
				ArrayNew[count1][4] = rs.getInt(5);
				count1 ++;
			}
			conn.close();
			return ArrayNew;
		}
		catch(Exception e) {
			System.out.println(e + "");
		}
		finally {
		}
		return null;
	}
}