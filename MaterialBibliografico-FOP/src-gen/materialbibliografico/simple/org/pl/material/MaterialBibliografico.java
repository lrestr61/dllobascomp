package materialbibliografico.simple.org.pl.material;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import javax.swing.border.EmptyBorder;
import materialbibliografico.simple.org.pl.material.bd.Conexion;
import materialbibliografico.simple.org.pl.material.dto.Material;
import materialbibliografico.simple.org.pl.material.dto.MaterialDAOMySQL;
/*** added by dMaterialBibliograficoSimple
 */
public class MaterialBibliografico extends JPanel {
	private JTextField textFieldTitulo;
	private JTextField textFieldCodigo;
	private JTextField textFieldAutor;
	private JTextField textFieldAnio;
	private JLabel lblAnio;
	public MaterialBibliografico() {
		super(new GridLayout(0, 1));
		add();
	}
	public void add() {
		JTabbedPane tabbedPane = new JTabbedPane();
		Object [] [] data1 = null;
		String [] columnNames = {
			"ID", "TITULO", "CODIGO", "AUTOR", "A�O"
		};
		try {
			data1 = MaterialDAOMySQL.obtenerTodas();
		}
		catch(Exception e) {
			System.out.println(e);
		}
		JTable table = new JTable(data1, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		ImageIcon icon =
		createImageIcon("/MaterialBibliografico-FOP/images/middle.gif");
		JComponent panel1 = new JPanel();
		JComponent panel2 = new JPanel();
		JComponent panel3 = new JPanel();
		JComponent panel4 = new JPanel();
		tabbedPane.addTab("Listar Material", icon, panel1);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		panel1.setLayout(new BorderLayout());
		panel1.add(table.getTableHeader(), BorderLayout.PAGE_START);
		panel1.add(table, BorderLayout.CENTER);
		add(tabbedPane);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	}
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = MaterialBibliografico.class.getResource(path);
		if(imgURL != null) {
			return new ImageIcon(imgURL);
		}
		else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
	public static void main(String [] args) {
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					UIManager.put("swing.boldMetal", Boolean.FALSE);
					createAndShowGUI();
				}
			});
	}
	private static void createAndShowGUI() {
		JFrame frame = new JFrame("Material Bibliogr�fico");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new MaterialBibliografico(), BorderLayout.CENTER);
		frame.setPreferredSize(new Dimension(600, 500));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}