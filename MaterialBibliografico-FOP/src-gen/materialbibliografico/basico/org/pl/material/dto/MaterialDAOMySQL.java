package materialbibliografico.basico.org.pl.material.dto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import materialbibliografico.basico.org.pl.material.bd.Conexion;
/*** added by dMaterialDAOMySQL* modified by dListarMaterial* modified by
dModificarMaterial
 */
public class MaterialDAOMySQL {
	static Statement st = null;
	PreparedStatement ps = null;
	static ResultSet rs = null;
	static Connection conn = null;
	Conexion conexion = null;
	/*** added by dListarMaterial
	 */
	public static Object [] [] obtenerTodas() {
		try {
			int count1 = 0;
			conn = Conexion.getConexion();
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM materialbibliografico");
			Object [] [] ArrayNew = new Object[30][5];
			while(rs.next()) {
				ArrayNew[count1][0] = rs.getInt(1);
				ArrayNew[count1][1] = rs.getString(2);
				ArrayNew[count1][2] = rs.getInt(3);
				ArrayNew[count1][3] = rs.getString(4);
				ArrayNew[count1][4] = rs.getInt(5);
				count1 ++;
			}
			conn.close();
			return ArrayNew;
		}
		catch(Exception e) {
			System.out.println(e + "");
		}
		finally {
		}
		return null;
	}
	/*** added by dModificarMaterial
	 */
	public static void modificar(Integer id, String titulo, Integer codigo,
		String autor, Integer anho) throws SQLException {
		try {
			conn = Conexion.getConexion();
			String query =
			"UPDATE materialbibliografico set titulo=?,codigo=?,autor=?,anio=? WHERE idMaterialBibliografico=?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps = conn.prepareStatement(query);
			ps.setString(1, titulo);
			ps.setInt(2, codigo);
			ps.setString(3, autor);
			ps.setInt(4, anho);
			ps.setInt(5, id);
			ps.executeUpdate();
			ps.close();
		}
		catch(SQLException se) {
			throw se;
		}
	}
	/*** added by dModificarMaterial
	 */
	public static Object [] [] obtenerPorId(Integer id) {
		try {
			int count1 = 0;
			conn = Conexion.getConexion();
			st = conn.createStatement();
			String query =
			"SELECT * FROM materialbibliografico WHERE idMaterialBibliografico= ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			Object [] [] ArrayNew = new Object[1][5];
			if(rs.next()) {
				ArrayNew[count1][0] = rs.getInt(1);
				ArrayNew[count1][1] = rs.getString(2);
				ArrayNew[count1][2] = rs.getInt(3);
				ArrayNew[count1][3] = rs.getString(4);
				ArrayNew[count1][4] = rs.getInt(5);
			}
			conn.close();
			System.out.println("Array" + ArrayNew[0][0]);
			return ArrayNew;
		}
		catch(Exception e) {
			System.out.println(e + "");
		}
		finally {
		}
		return null;
	}
}