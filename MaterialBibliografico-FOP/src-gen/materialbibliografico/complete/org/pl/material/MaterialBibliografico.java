package materialbibliografico.complete.org.pl.material;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import javax.swing.border.EmptyBorder;
import materialbibliografico.complete.org.pl.material.bd.Conexion;
import materialbibliografico.complete.org.pl.material.dto.Material;
import materialbibliografico.complete.org.pl.material.dto.MaterialDAOMySQL;
/*** added by dMaterialBibliograficoComplete
 */
public class MaterialBibliografico extends JPanel {
	private JTextField textFieldTitulo;
	private JTextField textFieldCodigo;
	private JTextField textFieldAutor;
	private JTextField textFieldAnio;
	private JLabel lblAnio;
	public MaterialBibliografico() {
		super(new GridLayout(0, 1));
		add();
	}
	public void add() {
		JTabbedPane tabbedPane = new JTabbedPane();
		Object [] [] data1 = null;
		String [] columnNames = {
			"ID", "TITULO", "CODIGO", "AUTOR", "A�O"
		};
		try {
			data1 = MaterialDAOMySQL.obtenerTodas();
		}
		catch(Exception e) {
			System.out.println(e);
		}
		JTable table = new JTable(data1, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		ImageIcon icon =
		createImageIcon("/MaterialBibliografico-FOP/images/middle.gif");
		JComponent panel1 = new JPanel();
		JComponent panel2 = new JPanel();
		JComponent panel3 = new JPanel();
		JComponent panel4 = new JPanel();
		tabbedPane.addTab("Listar Material", icon, panel1);
		tabbedPane.addTab("Agregar Material", icon, panel2);
		tabbedPane.addTab("Modificar Material", icon, panel3);
		tabbedPane.addTab("Eliminar Material", icon, panel4);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		panel1.setLayout(new BorderLayout());
		panel1.add(table.getTableHeader(), BorderLayout.PAGE_START);
		panel1.add(table, BorderLayout.CENTER);
		add(tabbedPane);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		panel2.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel2.setLayout(null);
		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setBounds(100, 49, 61, 16);
		panel2.add(lblTitulo);
		JLabel lblCodigo = new JLabel("C�digo");
		lblCodigo.setBounds(100, 89, 61, 16);
		panel2.add(lblCodigo);
		JLabel lblAutor = new JLabel("Autor");
		lblAutor.setBounds(100, 131, 61, 16);
		panel2.add(lblAutor);
		lblAnio = new JLabel("A�o");
		lblAnio.setBounds(100, 170, 61, 16);
		panel2.add(lblAnio);
		textFieldTitulo = new JTextField();
		textFieldTitulo.setBounds(187, 44, 130, 26);
		panel2.add(textFieldTitulo);
		textFieldTitulo.setColumns(10);
		textFieldCodigo = new JTextField();
		textFieldCodigo.setBounds(187, 84, 130, 26);
		panel2.add(textFieldCodigo);
		textFieldCodigo.setColumns(10);
		textFieldAutor = new JTextField();
		textFieldAutor.setBounds(187, 126, 130, 26);
		panel2.add(textFieldAutor);
		textFieldAutor.setColumns(10);
		textFieldAnio = new JTextField();
		textFieldAnio.setBounds(187, 165, 130, 26);
		panel2.add(textFieldAnio);
		textFieldAnio.setColumns(10);
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(160, 200, 117, 29);
		panel2.add(btnAgregar);
		btnAgregar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String titulo = textFieldTitulo.getText();
					int codigo = Integer.parseInt(textFieldCodigo.getText());
					String autor = textFieldAutor.getText();
					int anio = Integer.parseInt(textFieldAnio.getText());
					try {
						MaterialDAOMySQL.agregar(titulo, codigo, autor, anio);
						JOptionPane.showMessageDialog(null, "Registro Guardado con exito");
					}
					catch(SQLException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null,
							"No se logr� guardar en base de datos");
					}
				}
			});
		JLabel iddeletetext = new JLabel("Ingrese el ID del elemento a eliminar:");
		JTextField iddelete = new JTextField();
		JButton deletebutton = new JButton("Eliminar Material");
		iddelete.setPreferredSize(new Dimension(200, 25));
		panel4.add(iddeletetext);
		panel4.add(iddelete);
		panel4.add(deletebutton, BorderLayout.CENTER);
		deletebutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						int iiddelete = Integer.parseInt(iddelete.getText());
						System.out.println("Integer is: " + iiddelete);
						Object [] [] data2 = MaterialDAOMySQL.obtenerPorId(iiddelete);
						if(data2[0][0] == null) {
							JOptionPane.showMessageDialog(null,
								"El valor ingresado no existe en la base de datos");
						}
						else {
							try {
								MaterialDAOMySQL.eliminar(iiddelete);
								JOptionPane.showMessageDialog(null,
									"Material bibliogr�fico eliminado con exito");
							}
							catch(SQLException e) {
								e.printStackTrace();
							}
						}
					}
					catch(NumberFormatException ex) {
						System.out.println("Not a number");
						JOptionPane.showMessageDialog(null,
							"El valor ingresado no es un n�mero");
					}
				}
			});
		panel3.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel3.setLayout(null);
		JLabel lblTitle = new JLabel("Titulo");
		lblTitle.setBounds(100, 85, 61, 16);
		panel3.add(lblTitle);
		JLabel lblCode = new JLabel("C�digo");
		lblCode.setBounds(100, 125, 61, 16);
		panel3.add(lblCode);
		JLabel lblAuthor = new JLabel("Autor");
		lblAuthor.setBounds(100, 165, 61, 16);
		panel3.add(lblAuthor);
		lblAnio = new JLabel("A�o");
		lblAnio.setBounds(100, 205, 61, 16);
		panel3.add(lblAnio);
		JTextField textTitulo = new JTextField();
		textTitulo.setBounds(187, 80, 130, 26);
		panel3.add(textTitulo);
		textTitulo.setColumns(10);
		JTextField textCodigo = new JTextField();
		textCodigo.setBounds(187, 120, 130, 26);
		panel3.add(textCodigo);
		textCodigo.setColumns(10);
		JTextField textAutor = new JTextField();
		textAutor.setBounds(187, 160, 130, 26);
		panel3.add(textAutor);
		textAutor.setColumns(10);
		JTextField textAnio = new JTextField();
		textAnio.setBounds(187, 200, 130, 26);
		panel3.add(textAnio);
		textAnio.setColumns(10);
		JLabel idModifyText = new JLabel("Ingrese el ID del elemento a modificar:");
		idModifyText.setBounds(100, 20, 300, 25);
		JTextField idModify = new JTextField();
		idModify.setBounds(330, 20, 80, 25);
		JButton btnSearch = new JButton("Buscar");
		btnSearch.setBounds(430, 20, 80, 25);
		idModify.setPreferredSize(new Dimension(100, 25));
		panel3.add(idModifyText);
		panel3.add(idModify);
		panel3.add(btnSearch, BorderLayout.CENTER);
		btnSearch.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						int iIdModify = Integer.parseInt(idModify.getText());
						System.out.println("Integer is: " + iIdModify);
						Object [] [] data2 = MaterialDAOMySQL.obtenerPorId(iIdModify);
						System.out.println("dssss" + data2);
						if(data2[0][0] == null) {
							JOptionPane.showMessageDialog(null,
								"El valor ingresado no existe en la base de datos");
						}
						else {
							textTitulo.setText(data2[0][1].toString());
							textCodigo.setText(data2[0][2].toString());
							textAutor.setText(data2[0][3].toString());
							textAnio.setText(data2[0][4].toString());
						}
					}
					catch(NumberFormatException ex) {
						System.out.println("Not a number");
						JOptionPane.showMessageDialog(null,
							"El valor ingresado no es un n�mero");
					}
				}
			});
		JButton btnModify = new JButton("Guardar cambios");
		btnModify.setBounds(360, 300, 150, 25);
		panel3.add(btnModify, BorderLayout.CENTER);
		btnModify.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						int iIdModify = Integer.parseInt(idModify.getText());
						System.out.println("Integer is: " + iIdModify);
						String titleMod = textTitulo.getText();
						int codeMod = Integer.parseInt(textCodigo.getText());
						String authorMod = textAutor.getText();
						int anhoMod = Integer.parseInt(textAnio.getText());
						MaterialDAOMySQL.modificar(iIdModify, titleMod, codeMod, authorMod,
							anhoMod);
						JOptionPane.showMessageDialog(null,
							"El registro ha sido modificado exitosamente.");
					}
					catch(NumberFormatException ex) {
						System.out.println("Not a number");
						JOptionPane.showMessageDialog(null,
							"El valor ingresado no es un n�mero");
					}
					catch(SQLException e) {
						e.printStackTrace();
					}
				}
			});
	}
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = MaterialBibliografico.class.getResource(path);
		if(imgURL != null) {
			return new ImageIcon(imgURL);
		}
		else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
	public static void main(String [] args) {
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					UIManager.put("swing.boldMetal", Boolean.FALSE);
					createAndShowGUI();
				}
			});
	}
	private static void createAndShowGUI() {
		JFrame frame = new JFrame("Material Bibliogr�fico");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new MaterialBibliografico(), BorderLayout.CENTER);
		frame.setPreferredSize(new Dimension(600, 500));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}