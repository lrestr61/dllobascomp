package materialbibliografico.complete.org.pl.material.dto;

/*** added by dMaterial
 */
public class Material {
	public Integer id;
	public String titulo;
	public Integer codigo;
	public String autor;
	public Integer ano;
	public Material(int int1, String string, int int2, String string2, int int3)
	{
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Integer getAno() {
		return ano;
	}
}