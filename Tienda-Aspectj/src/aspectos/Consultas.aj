package aspectos;

import java.util.List;

import org.pl.eshop.dto.Categoria;

public aspect Consultas {
	String mensajeAutorizacion = "Se esta consultando de la bd.** ** ** ** ** ** ** ** ** ** ** **";
	
	pointcut consulta() : call(List<Categoria> *.obtenerTodas()) || call(Categoria *.obtenerPorId(..));
	
	//advice
	before() : consulta(){
		System.out.println(mensajeAutorizacion);
	}
}
