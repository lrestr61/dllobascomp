package aspectos;

public aspect Eliminaciones {
	pointcut eliminando() : call(* *.eliminar(..));
	
	//advice
	before() : eliminando(){
		System.out.println("Se va a eliminar información de la base de datos.** ** ** ** ** ** ** ** ** ** ** **");
	}
}
