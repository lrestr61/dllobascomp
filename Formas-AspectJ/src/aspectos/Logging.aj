package aspectos;

public aspect Logging {
	pointcut llamada() : call(double *.area()) || call(double *.perimetro());
	
	//advice
	before() : llamada(){
		System.out.println("Se llama el método área o perímetro.");
	}
}
